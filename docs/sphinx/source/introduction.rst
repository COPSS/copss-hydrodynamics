.. _introductions:

Introductions
===============
**COPSS**
---------

**COPSS** (Continuum Particle Simulation Software) is an open source software for
continuum-particle simulations. The package is designed to be easy to use, extensible,
and scalable. It currently includes three modules, COPSS-Hydrodynamics
to solve hydrodynamic interactions in confined suspensions, COPSS-Polarization to solve electrostatic
interactions in heterogeneous dielectric media, and COPSS-CMA-ES to inversely solve for charges on
dielectric particles from particle trajectories. All of these code repositories can be found `here
<https://bitbucket.org/COPSS/workspace/projects/>`_

**COPSS-Hydrodynamics**
----------------------------

**COPSS-Hydrodynamics** solves the hydrodynamic interactions in confined suspensions
by directly solve the Stokes equation. It is based on an efficient O(N) computational
approach to model the dynamics of hydrodynamically interacting Brownian or non-Brownian
particles in arbitrary geometries. A parallel finite element Stokes' solver is the center
of the algorithm.
