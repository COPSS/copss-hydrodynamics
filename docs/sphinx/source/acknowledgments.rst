.. _acknowledgments:

Acknowledgments
===================

(In Alphabetical order)

This work was supported by MICCoM, as part of the Com-
putational Materials Sciences Program funded by the U.S. De-
partment of Energy, Office of Science, Basic Energy Sciences,
Materials Sciences and Engineering Division and by the U.S.
Department of Energy, Office of Science and Advanced Scientific
Computing Research.


Principal Investigator
-----------------------

Professor Juan J. de Pablo


Core Development
-----------------

`Jiyuan Li <jyli@uchicago.edu>`_ (PhD student)

`Dr. Xikai Jiang <jxk28@msn.com>`_

Current Contributors
--------------------
Dr. Abhinendra Singh

Professor Juan P. Hernandez-Ortiz

Dr. Ming Han

Viviana Palacio Betancur (PhD student)

Past Contributors
-----------------

Michael Quevillon (PhD student)

Dr. Hythem Sidky

Dr. Xujun Zhao
