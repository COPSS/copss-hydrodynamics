
**COPSS-Hydrodynamics 0.4.0**
================================
(May 2020)

**COPSS-Hydrodynamics** solves the hydrodynamic interactions in confined suspensions by directly solve the Stokes equation. It is based on an efficient O(N) computational approach to model the dynamics of hydrodynamically interacting Brownian or non-Brownian particles in arbitrary geometries. A parallel finite element Stokes solver is the center of the algorithm. 


**Installation**
-------------------------------------------
**COPSS** is written on the [LIBMESH](http://libmesh.github.io/) framework. It also requires [PETSc](https://www.mcs.anl.gov/petsc/index.html) for parallel linear equation solvers and [SLEPc](http://slepc.upv.es/) for scalable Eigenvalue computations. Before installing **COPSS**, you need to install **LIBMESH** together with **PETSc** and **SLEPc**. To achieve the best parallel performance of COPSS, we suggest install all the dependent environment on a Linux cluster environment.

### 0. System environment prep

Load or compile

 - [CMAKE](https://cmake.org/) (e.g., but not necessarily, version 3.6.2)
 - [GCC](https://gcc.gnu.org/) (e.g., but not necessarily, version 6.2)
 - [PYTHON](https://www.python.org/) (python 2)
 - [OPENMPI](https://www.open-mpi.org/) (e.g., but not necessarily, version 2.0.1)

### 1. Install PETSc

 - Download PETSC latest release ( version 3.7.4 or later ) from [PETSc download](https://www.mcs.anl.gov/petsc/download/index.html), or git clone PETSc repository:
	 
	 - `mkdir $HOME/projects/`
	 
	 - `cd $HOME/projects/`
	 
	 - `git clone -b maint https://bitbucket.org/petsc/petsc petsc`

 - Configure PETSc: 
 
	 - `cd $HOME/projects/petsc`
	 
	 - `./configure --with-cc=mpicc --with-cxx=mpicxx --with-mpiexec=mpiexec --with-fc=mpif90 
	   --download-fblaslapack --download-scalapack --download-mumps --download-superlu_dist 
           --download-hypre --download-ml --download-parmetis --download-metis --download-triangle 
           --download-chaco --with-debugging=0`
	 
	 - And then follow the instructions on screen to install and test the package.** 

 - Export environment variables:
 
    	 - `export PETSC_DIR=/path/to/PETSC`
    
    	 - `export PETSC_ARCH=PETSC_ARCH_NAME`
	
	 **Add the codes above to ~/.bashrc and `source ~/.bashrc` before next step. (`/path/to/PETSC` and `PETSC_ARCH_NAME` can be found on the screen after installation.)**

If you meet any trouble, please refer to [PETSC installation](https://www.mcs.anl.gov/petsc/documentation/installation.html).
	
### 2. Install SLEPc

 - Download SLEPC latest release (version 3.7.3 or later) from [SLEPc download](http://slepc.upv.es/download/download.htm), or git clone PETSc repository:
	 
	 - `cd $HOME/projects/`
	
	 - `git clone -b maint https://bitbucket.org/slepc/slepc slepc`
	 
 - Configure PETSc: 
 
	 - `cd $HOME/projects/slepc`
	 
	 - `./configure`
	 
	 **And then follow the instructions on screen to install the package**
	  
 - Export environment variables:
    
    	 - `export SLEPC_DIR=/path/to/SLEPC`
	
    	 - `export SLEPC_ARCH=SLEPC_ARCH_NAME`
	
	 **Add the codes above to ~/.bashrc and `source ~/.bashrc` before next step. (`/path/to/SLEPC` and `SLEPC_ARCH_NAME` can be found on the screen after installation.)**
	  
 - Test the package (not necessary but recommended)
 
	 - `make test`

If you meet any trouble, please refer to [SLEPC installation](http://slepc.upv.es/documentation/instal.htm).

### 3. Install LIBMESH

 - Download LIBMESH latest release ( version 1.1.0 or later ) from [LIBMESH download](https://github.com/libMesh/libmesh/releases), or git clone PETSc repository:
	 
	 - `cd $HOME/projects/`
	 
	 - `git clone git://github.com/libMesh/libmesh.git`
	 
 - Build LIBMESH: 
 
	 - `cd $HOME/projects/libmesh`
	 
	 - `./configure -prefix=$HOME/projects/libmesh/libmesh-opt --enable-optional --enable-vtk  --enable-gzstream --enable-trilinos --disable-strict-lgpl --enable-laspack --enable-capnproto --enable-trilinos --enable-nodeconstraint --enable-perflog --enable-ifem --enable-petsc --enable-blocked-storage --enable-slepc --enable-unique-id --enable-unique-ptr --enable-parmesh 2>&1  | tee my_config_output_opt.txt`

	(Read the configuration output, make sure **PETSC** and **SLEPC** is enabled).
	 
	 **And then follow the instructions on screen to install and test the package.** 
	 
 - Export environment variables:
    
    	 - `export LIBMESH_DIR=/path/to/LIBMESH`
	
	 **Add the codes above to ~/.bashrc and `source ~/.bashrc` before next step. (`/path/to/PETSC`can be found on the screen after installation.)**

If you meet any trouble, please refer to [LIBMESH installation](https://libmesh.github.io/installation.html), or reach out to **LIBMESH** community for help.

### 4. Install COPSS-hydrodynamics

 - Download the latest COPSS codes

	 - `cd /path/to/where/you/want/to/install/copss`
	 
	 - `git clone https://bitbucket.org/COPSS/copss-hydrodynamics-public.git`

 - Compile the codes
 
	- `cd src/` 
    
    	- `make package=POINTPARICLE (for point particle systems)  Or make package=RIGIDPARTICLE (for rigid particle systems)`	 

 - Run an sedimentation example of rigid particles
 
    - `cd examples/general_rigid_particle/sedimentation_benchmark/`

    - modify run.sh dependending on your system

    - `bash run.sh`
	  
**Build documentation**
-------------------------------------------
* **Doxygen Documentation**

    After you have build **COPSS-Hydrodynamics** you can further build the documentation from doxygen/ directory. Make sure you have [Doxygen](http://www.stack.nl/~dimitri/doxygen/) ready:
    
    * `cd docs/doxygen/`
    * `doxygen Doxyfile.bak`

    Then you can view the documentation in any browser:
    * `google-chrome html/index.html`

* **Sphinx documentation**

    You can also build Sphinx documentation to checkout the manual/tutorials of COPSS. Make sure you have [Sphinx](https://www.sphinx-doc.org/en/master/usage/installation.html) ready. 
    
    * `cd docs/sphinx`
    * `make html`

    Then you can view the documentation in any browser:
    
    * `google-chrome build/html/index.html`
    

What is next
-------------------------------------------

 - **Poisson Module**: develop Accelerated Poisson Solver using General Geometry Ewald-like Method (GGEM) to solve electrostatics in confined geometries. The new solver is able to solve the electrostatic potential field induced by charged particles and electrolytes under both Dirichlet boundary conditions (BC), i.e., fixed potential at boundaries, and Neumann BC, i.e., fixed surface charge density at boundaries.
 - **Nernst-Planck Module**: develop Nernst-Planck solver to calculate the time- and spatial-dependent distribution of charged chemical species in a fluid medium. The solver considers both diffusion of chemical species and convection induced by electrostatic forces and fluid.
 - **Multi-Physics coupling**: Develop a general framework to enable the coupling between two or more physics modules from Electrostatic, Hydrodynamics and Integrator modules. The potential applications of these couplings include study of transport of ionic species in confinement, transport of colloids in confined ionic fluid, etc.
 - Coupling COPSS with LAMMPS to enable deterministic dynamic simulations of polarizable particles using the force fields, fixes and integrators embedded in LAMMPS.
 - Coupling COPSS-Hydrodynamics with [SSAGES](https://github.com/MICCoM/SSAGES-public) to enable enhanced sampling of hydrodynamically-interacting particles.
 - **GPU Support**: Add GPU support to COPSS utilizing the GPU and hybrid MPI-GPU parallelism features in PETSC13. This GPU support provides alternative high-performance and low-cost linear solver solutions.
 - etc...

**Contribution**
-------------------------------------------
Hydrodynamic and electrostatic interactions are ubiquitous in nature and technology, including but not limited to colloidal, biological, granular systems, etc. **COPSS** is trying to solve these problems in a efficient and scalable manner. Contributions on the code structure, parallel-performance, applications are much appreciated. 

**Contact**
-------------------------------------------
 
 -  If you need help, have any questions or comments, join our [mailing list](copss-users@googlegroups.com)

     **GMail users**: just click "Join group" button

     **Everyone else**: send an email to [copss-users+subscribe@googlegroups.com](Link URL)

**Algorithm**
-------------------------------------------
 Our method paper has details on the implementation and tests of this code: [Parallel $O(N)$ Stokes solver towards scalable Brownian dynamics of hydrodynamically interacting objects in general geometries](https://aip.scitation.org/doi/10.1063/1.4989545)


Main contributors
-------------------------------------------
(In Alphabetical Order)

**Principal Investigator**

- Professor Juan J. de Pablo

**Current Contributors**

- Dr. Abhinendra Singh

- Jiyuan Li ([Contact](jyli@uchicago.edu)) (PhD student)

- Dr. Johnson Dhanasekaran ([Contact](johnson739@uchicago.edu))

- Professor Juan P. Hernandez-Ortiz

- Dr. Xikai Jiang ([Contact](jxk28@msn.com))

**Past Contributors**

- Michael Quevillon (PhD student)

- Dr. Hythem Sidky

- Dr. Xujun Zhao

- Dr. Ming Han

- Viviana Palacio Betancur (PhD student)

**License**
-------------------------------------------
 
The codes are open-source and distributed under the GNU GPL license, and may not be used for any commercial or for-profit purposes without our permission.



**Release history of COPSS**
-------------------------------------------

 - 0.4.0 (Release COPSS-Hydrodynamics for Rigid Particle model based on Immersed Boundary Method)
 
 - 0.3.0 (Release COPSS-CMA-ES module)

 - 0.2.0 (Release COPSS-Hydrodynamics module)

 - 0.1.0 (Release COPSS-Polarization module)